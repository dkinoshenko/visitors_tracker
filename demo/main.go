package main

import (
	"github.com/sirupsen/logrus"
	visitorstracker "gitlab.com/dkinoshenko/visitors_tracker"
)

func main() {
	logrus.SetLevel(logrus.DebugLevel)

	visitorstracker.StartServer(visitorstracker.ServerOpt{
		LogPort: 5000,
		LogPath: "/logs",

		MetricPort: 9102,
		MetricPath: "/metrics",
		MetricName: "unique_ip_addresses",
	})
}
