package visitorstracker

// LogMessage defines an input log message (which contains IP address).
type LogMessage struct {
	IP string `json:"ip"`
}

// IPAddressTracker tracks IP addresses.
type IPAddressTracker interface {
	AppendIP(ip string) bool
}

// VisitorLogger handles logs of visitors.
type VisitorLogger struct {
	tracker IPAddressTracker
}

// NewVisitorLogger creates a new visitor logger.
func NewVisitorLogger(tracker IPAddressTracker) *VisitorLogger {
	return &VisitorLogger{
		tracker: tracker,
	}
}

// TrackVisitor tracks a new visitor.
func (li *VisitorLogger) TrackVisitor(vLog LogMessage) bool {
	return li.tracker.AppendIP(vLog.IP)
}
