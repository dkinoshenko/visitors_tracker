package visitorstracker

import (
	"github.com/axiomhq/hyperloglog"
	"github.com/sirupsen/logrus"
	"sync"
)

// IPUniqueAddressTracker tracks unique IP addresses based on HyperLogLog algorithm.
type IPUniqueAddressTracker struct {
	mu sync.Mutex
	sk *hyperloglog.Sketch
}

// IPUniqueAddressTracker creates a new IPUniqueAddressTracker
func NewUniqueIPAddressTracker() *IPUniqueAddressTracker {
	return &IPUniqueAddressTracker{
		sk: hyperloglog.New16(),
	}
}

// AppendIP appends a new IP. It returns true if the IP address is already tracked, false otherwise.
func (ipt *IPUniqueAddressTracker) AppendIP(ip string) bool {
	ipt.mu.Lock()
	defer ipt.mu.Unlock()
	// todo idea: we can optimize to put into sketch 4 bytes of IP address only.
	logrus.Debugf("To track IP: %s", ip)

	return ipt.sk.Insert([]byte(ip))
}
