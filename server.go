package visitorstracker

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

type ServerOpt struct {
	LogPort int
	LogPath string

	MetricPort int
	MetricPath string
	MetricName string
}

func StartServer(opt ServerOpt) {
	inputLog := mux.NewRouter()
	outputMetric := mux.NewRouter()

	tracker := NewUniqueIPAddressTracker()
	vLogger := NewVisitorLogger(tracker)

	uniqueIPcounter := prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: "golang",
			Name:      opt.MetricName,
		})
	prometheus.MustRegister(uniqueIPcounter)

	inputLog.HandleFunc(opt.LogPath, func(rw http.ResponseWriter, req *http.Request) {
		var message LogMessage

		err := json.NewDecoder(req.Body).Decode(&message)
		if err != nil {
			http.Error(rw, err.Error(), http.StatusBadRequest)
		}

		if isNew := vLogger.TrackVisitor(message); isNew {
			uniqueIPcounter.Inc()
		}
	})

	outputMetric.HandleFunc(opt.MetricPath, func(rw http.ResponseWriter, req *http.Request) {
		promhttp.Handler().ServeHTTP(rw, req)
	})

	go http.ListenAndServe(":"+strconv.Itoa(opt.LogPort), inputLog)
	go http.ListenAndServe(":"+strconv.Itoa(opt.MetricPort), outputMetric)

	logrus.Infof("Server is started")

	select {} // block forever to prevent exiting
}
