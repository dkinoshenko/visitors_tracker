package visitorstracker

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"math/rand"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestNewIPAddressTracker(t *testing.T) {
	tracker := NewUniqueIPAddressTracker()
	require.NotNil(t, tracker)

	require.True(t, tracker.AppendIP("11.22.33.44"))
	require.True(t, tracker.AppendIP("11.22.33.45"))
	require.True(t, tracker.AppendIP("11.22.33.46"))
	require.True(t, tracker.AppendIP("11.22.33.41"))
	require.True(t, tracker.AppendIP("11.22.33.43"))
	require.False(t, tracker.AppendIP("11.22.33.41")) // duplicate
	require.False(t, tracker.AppendIP("11.22.33.46")) // duplicate
}

func BenchmarkNewUniqueIPAddressTracker(b *testing.B) {
	benchdata := generateData(1_000)

	b.ReportAllocs()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		tracker := NewUniqueIPAddressTracker()
		present := 0
		for j := 0; j < len(benchdata); j++ {
			if ok := tracker.AppendIP(benchdata[j]); !ok {
				present++
			}
		}
		fmt.Printf("present=%d\n", present)
	}
	b.StopTimer()
}

func generateData(n int) []string {
	res := make([]string, n)

	rand.Seed(time.Now().UnixNano())
	generateIPPart := func() string {
		return strconv.Itoa(rand.Intn(256))
	}

	for i := 0; i < n; i++ {
		time.Sleep(3 * time.Millisecond)
		res[i] = strings.Join([]string{
			// fix the first two parts to allow repetition of IP addresses.
			"192",
			"168",
			generateIPPart(),
			generateIPPart(),
		}, ".")
	}

	return res
}
