package visitorstracker

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestNewIPLogInput(t *testing.T) {
	tracker := newTestIPTracker()
	input := NewVisitorLogger(tracker)

	input.TrackVisitor(newVisitor("11.22.33.44"))
	require.Equal(t, 1, tracker.card())

	input.TrackVisitor(newVisitor("11.22.33.45"))
	require.Equal(t, 2, tracker.card())

	input.TrackVisitor(newVisitor("11.22.33.46"))
	require.Equal(t, 3, tracker.card())

	input.TrackVisitor(newVisitor("11.22.33.41"))
	require.Equal(t, 4, tracker.card())

	input.TrackVisitor(newVisitor("11.22.33.43"))
	require.Equal(t, 5, tracker.card())

	input.TrackVisitor(newVisitor("11.22.33.41"))
	require.Equal(t, 5, tracker.card())

	input.TrackVisitor(newVisitor("11.22.33.46"))
	require.Equal(t, 5, tracker.card())
}

func newVisitor(ip string) LogMessage {
	return LogMessage{ip}
}

type testIPTracker struct {
	cache map[string]struct{}
}

func newTestIPTracker() *testIPTracker {
	return &testIPTracker{
		cache: make(map[string]struct{}),
	}
}

func (t *testIPTracker) AppendIP(ip string) bool {
	if _, ok := t.cache[ip]; ok {
		return false
	} else {
		t.cache[ip] = struct{}{}
		return true
	}
}

func (t *testIPTracker) card() int {
	return len(t.cache)
}
