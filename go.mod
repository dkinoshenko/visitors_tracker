module gitlab.com/dkinoshenko/visitors_tracker

go 1.14

require (
	github.com/axiomhq/hyperloglog v0.0.0-20191112132149-a4c4c47bc57f
	github.com/gorilla/mux v1.7.4
	github.com/prometheus/client_golang v1.7.1
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.6.1
)
